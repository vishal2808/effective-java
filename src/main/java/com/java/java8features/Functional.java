package com.java.java8features;

@FunctionalInterface
public interface Functional {
	void method();
	
	default String notRequired() {
		return "Default Implementation";
	}
}
