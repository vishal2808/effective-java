package com.java.java8features;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.swing.JButton;

/**
 * @author vishalsharma
 */
public class SimpleLambdaExample extends Frame {

	private static final long serialVersionUID = 1L;

	public static void main(String[] args) { 
		Arrays.asList( "a", "b", "d" ).forEach( e -> {
			System.out.print( e );
			System.out.println( e );
			} );
		Arrays.asList( "a", "b", "d" ).sort( ( e1, e2 ) -> e1.compareTo( e2 ) );
	}
    
}
