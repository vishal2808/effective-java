package com.java.executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author vishalsharma
 */
public class ExecutorService1 {
    public static void main (String args[]){
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        executorService.execute(new Runnable(){
            @Override
            public void run(){
                System.out.println("Asynchronous task");
            }
        });
        
        executorService.shutdown();
    }
}
