/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.java.executors;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author vishalsharma
 */
public class FutureObjectDemo {
    private static final ExecutorService threadPool = Executors.newFixedThreadPool(3);
    
    public static void main (String[] args) throws ExecutionException, InterruptedException {
        FactorialCalculator task = new FactorialCalculator(20);
        System.out.println("Submitting task ...");
        
        Future futureObject = threadPool.submit(task);
        System.out.println("Task is submitted!");
        
        while(!futureObject.isDone()){
            System.out.println("Task is not completed yet ...");
            Thread.sleep(50);
        }
        
        System.out.println("Task is complete! Here is the result ...");
        System.out.println(futureObject.get());
        
        threadPool.shutdown();
    }
    
    private static class FactorialCalculator implements Callable {

        private final int number;

        public FactorialCalculator(int number) {
            this.number = number;
        }
        
        @Override
        public Long call() {
            long output = 0;
            try {
                output = factorial(number);
            } catch (InterruptedException ex) {
                System.out.println("Exception occurred " + ex.getMessage());
            }
            return output;
        }
        
        private long factorial(long number) throws InterruptedException{
            if(number < 0) {
                throw new IllegalArgumentException();
            }
            long result = 1;
            while(number > 0){
                Thread.sleep(100);
                result = result*number;
                number--;
            }
            return result;
        }
    }
}
