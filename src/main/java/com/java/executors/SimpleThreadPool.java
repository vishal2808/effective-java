package com.java.executors;

import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vishalsharma
 */
public class SimpleThreadPool {

    private final WorkerThread[] workerThreads;
    private final LinkedList<Runnable> threadPool;


    public SimpleThreadPool(int numberOfThreads) {
        threadPool = new LinkedList<>();
        workerThreads = new WorkerThread[numberOfThreads];
        for (int i = 0; i < workerThreads.length; ++i) {
            workerThreads[i] = new WorkerThread();
            workerThreads[i].start();
        }
    }
    
    public void enqueue(Runnable r){
        synchronized(threadPool){
            threadPool.addLast(r);
            threadPool.notify();
        }
    }
    
    public class WorkerThread extends Thread {

        @Override
        public void run() {
            Runnable r;
            while(true){
                synchronized(threadPool){
                    while(threadPool.isEmpty()){
                        try {
                            threadPool.wait();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(SimpleThreadPool.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    r = threadPool.removeFirst();
                }
                try {
                    r.run();
                } catch(Exception e) {
                    Logger.getLogger(SimpleThreadPool.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }

}
